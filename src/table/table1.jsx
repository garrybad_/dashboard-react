import * as am5 from "@amcharts/amcharts5";
import * as am5xy from "@amcharts/amcharts5/xy";
import am5themes_Animated from "@amcharts/amcharts5/themes/Animated";
import React, {
    useCallback,
    useMemo,
    useRef,
    useState,
    StrictMode,
  } from 'react';
import { render } from "react-dom";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-community/styles/ag-grid.css";
import "ag-grid-community/styles/ag-theme-alpine.css";
import 'ag-grid-enterprise';

import "../assets/fonts/DMSans-Regular.ttf"
import "../assets/fonts/DMSans-Medium.ttf"
import "../assets/fonts/DMSans-Bold.ttf"

const Table1 = () => {
    const gridRef = useRef();
    const [rowData, setRowData] = useState();
  
    const [columnDefs] = useState([
        { field: 'athlete' },
        { field: 'age', maxWidth: 100 },
        { field: 'date', },
        { field: 'country' },
        { field: 'sport' },
        { field: 'gold' },
        { field: 'silver' },
        { field: 'bronze' },
        { field: 'total', filter: false },
    ]);

    const onGridReady = useCallback((params) => {
        fetch('https://www.ag-grid.com/example-assets/olympic-winners.json')
          .then((resp) => resp.json())
          .then((data) => setRowData(data));
    }, []);

    const onBtExport = useCallback(() => {
        gridRef.current.api.exportDataAsExcel();
    }, []);
  
    return (
        <>
            <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginBottom: '1vh'}}>
                <div style={{color: 'white', fontFamily: 'dm-bold' }}>Table Data</div>
                {/* <div style={{color: 'white', fontFamily: 'dm-bold' }}>Table Data</div> */}
                <button className='btn-export' onClick={onBtExport} type='button'>Export</button>
            </div>
            <div className="ag-theme-alpine" style={{ height: '45vh', width: '100%' }}>
                <AgGridReact 
                ref={gridRef}
                rowData={rowData} 
                columnDefs={columnDefs}
                onGridReady={onGridReady}
                className="ag-theme-alpine-dark"></AgGridReact>
            </div>
        </>
    );
  };

export default Table1;