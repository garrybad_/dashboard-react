import React from 'react'; 
import Chart1 from '../chart/chart1';
import Chart2 from '../chart/chart2';
import Chart3 from '../chart/chart3';
import Chart4 from '../chart/chart4';
import Chart5 from '../chart/chart5';
import "../assets/fonts/DMSans-Regular.ttf"
import "../assets/fonts/DMSans-Bold.ttf"
import "./pages2.css"
import { DatePicker, Space, ConfigProvider } from 'antd';
  
function Pages2 (){ 
    const { RangePicker } = DatePicker;
    return (
    <>
        <div className='wrap-bg' style={{width: '100%', padding: '2vh'}}>
            <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', padding: '0 0.5vh', marginBottom: '1vh'}}>
                <div style={{color: 'white', fontFamily: 'dm-bold', fontSize: '3vh'}}>Dashboard 1</div>
                {/* <div className='date-picker-wrap'>
                    <RangePicker />
                </div> */}
            </div>
            <div className="grid grid-cols-3" style={{marginBottom: '1vh'}}>
                <div style={{padding: '0 0.5vh'}}>
                <div style={{backgroundColor: '#252445', padding: '1vh', borderRadius: '8px'}}>
                    <div style={{textAlign: 'center', color: 'white', fontFamily: 'dm-bold'}}>Chart 1</div>
                    <Chart1 props={'chart1'} />
                </div>  
                </div>
                <div style={{padding: '0 0.5vh'}}>
                <div style={{backgroundColor: '#252445', padding: '1vh', borderRadius: '8px'}}>
                    <div style={{textAlign: 'center', color: 'white', fontFamily: 'dm-bold'}}>Chart 2</div>
                    <Chart2 props={'chart2'} />
                </div>
                </div>
                <div style={{padding: '0 0.5vh'}}>
                <div style={{backgroundColor: '#252445', padding: '1vh', borderRadius: '8px'}}>
                    <div style={{textAlign: 'center', color: 'white', fontFamily: 'dm-bold'}}>Chart 3</div>
                    <Chart3 props={'chart3'} />
                </div>
                </div>
            </div>
            <div className="grid grid-cols-2">
                <div style={{padding: '0 0.5vh'}}>
                <div style={{backgroundColor: '#252445', padding: '1vh', borderRadius: '8px'}}>
                    <div style={{textAlign: 'center', color: 'white', fontFamily: 'dm-bold'}}>Chart 4</div>
                    <Chart4 props={'chart4'} />
                </div>
                </div>
                <div style={{padding: '0 0.5vh'}}>
                <div style={{backgroundColor: '#252445', padding: '1vh', borderRadius: '8px'}}>
                    <div style={{textAlign: 'center', color: 'white', fontFamily: 'dm-bold'}}>Chart 5</div>
                    <Chart5 props={'chart5'} />
                </div>
                </div>
            </div>
        </div>
    </>)
} 
  
export default Pages2;