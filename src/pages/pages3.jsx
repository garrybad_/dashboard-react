import React from 'react'; 
import "./pages2.css"
import Chart1 from '../chart/chart1';
import Chart2 from '../chart/chart2';
import Chart4 from '../chart/chart4';
import Chart6 from '../chart/chart6';
import Chart7 from '../chart/chart7';
import Table1 from '../table/table1';
  
function Pages3 (){ 
    return(
    <>
        <div className='wrap-bg' style={{width: '100%', padding: '2vh'}}>
            <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', padding: '0 0.5vh', marginBottom: '1vh'}}>
                <div style={{color: 'white', fontFamily: 'dm-bold', fontSize: '3vh'}}>Dashboard 2</div>
                {/* <div className='date-picker-wrap'>
                    <RangePicker />
                </div> */}
            </div>
            <div className="grid grid-cols-3" style={{marginBottom: '1vh'}}>
                <div style={{padding: '0 0.5vh'}}>
                    <div style={{backgroundColor: '#252445', padding: '1vh', borderRadius: '8px'}}>
                        <div style={{textAlign: 'center', color: 'white', fontFamily: 'dm-bold'}}>Chart 6</div>
                        <Chart6 props={'chart6'} />
                    </div>  
                </div>
                <div className='col-span-2' style={{padding: '0 0.5vh'}}>
                    <div style={{backgroundColor: '#252445', padding: '1vh', borderRadius: '8px'}}>
                        <div style={{textAlign: 'center', color: 'white', fontFamily: 'dm-bold'}}>Chart 7</div>
                        <Chart7 props={'chart7'} />
                    </div>
                </div>
            </div>
            <div style={{backgroundColor: '#252445', padding: '2vh', borderRadius: '8px'}}>
                <Table1 />
            </div>
        </div>
    </>)
} 
  
export default Pages3;