import React from 'react'; 
import { useNavigate } from "react-router-dom";
  
function Pages1 (){ 
    let navigate = useNavigate(); 
    const routeChange1 = () =>{ 
        let path = `/pages2`; 
        navigate(path);
    }

    const routeChange2 = () =>{ 
        let path = `/pages3`; 
        navigate(path);
    }

    return(
        <>
            <div className='wrap-bg-home' style={{width: '100%', height: '100vh'}}>
                <div style={{width: '100%', height: '100%', display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', padding: '0 20%', textAlign: 'center'}}>
                    <div style={{fontFamily: 'dm-bold', color: 'white', fontSize: '10vh'}}>Boost your productivity with our intuitive and responsive dashboard</div>
                    <div style={{display: 'flex'}}>
                        <button className='btn-dash' onClick={routeChange1}>Dashboard 1</button>
                        <button className='btn-dash' onClick={routeChange2}>Dashboard 2</button>
                    </div>
                {/* <div style={{fontFamily: 'dm-reg', color: 'white'}}>Boost your productivity with our intuitive and responsive dashboard. Monitor performance in real-time, make quick decisions, and enjoy a superior user experience to take your business to the next level.</div> */}
                </div>
            </div>
        </>
    )
} 
  
export default Pages1;