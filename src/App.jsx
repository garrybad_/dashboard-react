import React, { Component, useLayoutEffect, useState } from "react";
import { BrowserRouter as Router,Routes, Route, Link } from 'react-router-dom'; 
import './App.css'
import Pages1 from "./pages/pages1";
import Pages2 from "./pages/pages2";
import Pages3 from "./pages/pages3";
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import 'bootstrap/dist/css/bootstrap.min.css';

function App(props) {
  let Links =[
    {name:"Pages 1", link:"/"},
    {name:"Pages 2", link:"/pages2"},
    {name:"Pages 3", link:"/pages3"},
  ];
  let [open,setOpen]=useState(false);

  return (
    <>
      <Navbar sticky="top" expand="lg" style={{backgroundColor: '#252445'}}>
        <Container>
          <Navbar.Brand href="#home" style={{fontFamily: 'dm-bold', color: 'white'}}>Quick View</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="justify-content-end" style={{ flex: 1}}>
              <Nav.Link href="/" style={{fontFamily: 'dm-bold', color: 'white'}}>Home</Nav.Link>
              <Nav.Link href="/pages2" style={{fontFamily: 'dm-bold', color: 'white'}}>Dashboard 1</Nav.Link>
              <Nav.Link href="/pages3" style={{fontFamily: 'dm-bold', color: 'white'}}>Dashboard 2</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <Router> 
          {/* <div className="App"> 
              <ul className="App-header"> 
              <li> 
                  <Link to="/">Pages 1</Link> 
              </li> 
              <li> 
                  <Link to="/pages2">Pages 2</Link> 
              </li> 
              <li> 
                  <Link to="/pages3">Pages 3</Link> 
              </li> 
              </ul> 
              <Routes> 
                      <Route exact path='/' element={< Pages1 />}></Route> 
                      <Route exact path='/pages2' element={< Pages2 />}></Route> 
                      <Route exact path='/pages3' element={< Pages3 />}></Route> 
              </Routes> 
          </div>  */}
          <Routes>
            <Route exact path='/' element={< Pages1 />}></Route> 
            <Route exact path='/pages2' element={< Pages2 />}></Route> 
            <Route exact path='/pages3' element={< Pages3 />}></Route> 
          </Routes> 
      </Router> 
    </>
  );
}

export default App;
