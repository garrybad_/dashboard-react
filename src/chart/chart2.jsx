import * as am5xy from "@amcharts/amcharts5/xy";
import am5themes_Animated from "@amcharts/amcharts5/themes/Animated";
import * as am5percent from "@amcharts/amcharts5/percent";
import * as am5 from "@amcharts/amcharts5";
import React, { Component, useLayoutEffect } from "react";
import "../assets/fonts/DMSans-Regular.ttf"
import "../assets/fonts/DMSans-Medium.ttf"
import "../assets/fonts/DMSans-Bold.ttf"

const Chart2 = ({props}) => {
    console.log('props now', props);
    useLayoutEffect(() => {
        let root = am5.Root.new(props);
        let myTheme = am5.Theme.new(root);

        // Set themes
        // https://www.amcharts.com/docs/v5/concepts/themes/
        root.setThemes([
            am5themes_Animated.new(root)
        ]);

        let cirka = root.container.children.push(
            am5percent.PieChart.new(root, {
            radius: am5.percent(40),
            isMeasured: false,
            y: 0,
            fontSize: '2vh'
            // paddingLeft: 30
            })
        );

        let cirkaSeries = cirka.series.push(
            am5percent.PieSeries.new(root, {
            name: "Series",
            categoryField: "country",
            valueField: "sales",
            // showTooltipOn: 'click' as any
            })
        );

        cirkaSeries.data.setAll([{
            country: " ",
            sales: 100
        }]);

        cirkaSeries.labels.template.setAll({
            forceHidden: true,
        });

        cirkaSeries.slices.template.setAll({
            shadowColor: am5.color('#AAA'),
            fillOpacity: 1,
            shadowBlur: 8,
            fill: am5.color('#252445'),
            strokeOpacity: 0,
            fontSize: "9vh",
            tooltipText: ''
        });

        cirkaSeries.slices.template.set("toggleKey", "none");

        let cirkaLabel = cirkaSeries.labelsContainer.children.push(am5.Label.new(root, {
            // const cirkaLabel = cirkaSeries.labelsContainer.children.push(am5.Label.new(root, {
            textAlign: 'center',
            oversizedBehavior: 'fit',
            // text: '[#FFF][fontFamily: Barlow; fontSize: 2vh; line-height: 1vh]',
            text: `[#FFF][fontFamily: dm-reg fontSize:2vh]Total[/]
[#FFF fontFamily: dm-bold fontSize:2.5vh]25[/]`,
            // text: `[#FFF][fontFamily:Roboto fontWeight:700 fontSize:2vh]Total PT[/]
            // [#FFF fontFamily:Roboto bold fontSize:2vh fontWeight:500]${this.totalvalue}[/]`,
            // +'[\]\n[#AAA][fontFamily: Barlow; fontSize: 1.5vh]100%[\]',
            centerY: am5.p50,
            centerX: am5.p50,
            populateText: true
        }));

        cirka.chartContainer.events.on('click', (ev) => {
            // this.urlservice.setData('view','event');
        });


        // Create chart
        // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/
        let chart = root.container.children.push(am5percent.PieChart.new(root, {
            layout: root.verticalLayout,
            innerRadius: am5.percent(70),
            radius: am5.percent(70),
        }));

        // Create series
        // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Series

        let series = chart.series.push(am5percent.PieSeries.new(root, {
            valueField: "value",
            categoryField: "category",
            alignLabels: false,
            fillField: "color"
        }));

        series.labels.template.setAll({
            textType: "regular",
            textAlign: 'center',
            // fontFamily: 'Roboto',
            inside: false,
            radius: 30,
            fill: am5.color(0xFFFFFF),
            populateText: true,
            text: "[fontFamily: 'dm-med' #FFF fontSize:1.5vh]{category}[/]\n[fontFamily: 'dm-bold' fontSize: 1.5vh]{valuePercentTotal.formatNumber('0.00')}%",
            forceHidden: false,
            layer: 1
        });

        // series.labels.template.adapters.add('text', (value: any, target: any) => {
        //   const warna = target.dataItem?.dataContext?.color?.toCSSHex();
        //   return '[' + warna + '; fontSize: 1.3vh; fontFamily: Roboto]' + '{valuePercentTotal.formatNumber("0.00")}%\n[#FFF; fontSize: 1.2vh; fontFamily: Roboto]{category}\n{value}';
        // });

        series.labels.template.states.create("aktif", {
            opacity: 1
        });

        series.labels.template.states.create("nonaktif", {
            opacity: 0
        });

        series.labels.template.setAll({
            interactive: true,
            stateAnimationDuration: 500,
            stateAnimationEasing: am5.ease.out(am5.ease.cubic)
        });

        // series.ticks.template.adapters.add('stroke', (value: any, target: any, ) => {
        //   //  console.log('halo', target);
        //   return target.dataItem.dataContext.color;
        // });

        series.slices.template.setAll({
            // cornerRadius: 50,
            // stroke: am5.color(0x0D1D45),
            tooltipText:
            "{category}: [bold]{valuePercentTotal.formatNumber('0.00')}%[/] ({value})"
        })

        let data = [
            { value: 10, category: "Low", color: '#41CFC6' },
            { value: 9, category: "Med", color: '#FFB766' },
            { value: 6, category: "High", color: '#F24040' },
        ];

        series.data.setAll(data);

        // Play initial series animation
        // https://www.amcharts.com/docs/v5/concepts/animations/#Animation_of_series
        series.appear(1000).then(x => {
            // temp1._logo._display.getCanvas()
            // temp2.style.display = "none"
            root._logo?.hide();
            (root)._handleLogo = () => {
            // console.log('-');
            };
        });

        return () => {
            root.dispose();
        };
    }, []);

    return(
        <>
            <div id={props} style={{ width: "100%", height: "45vh" }}></div>
        </>
    )
}

export default Chart2