import * as am5 from "@amcharts/amcharts5";
import * as am5xy from "@amcharts/amcharts5/xy";
import am5themes_Animated from "@amcharts/amcharts5/themes/Animated";
import React, { Component, useLayoutEffect } from "react";
import "../assets/fonts/DMSans-Regular.ttf"
import "../assets/fonts/DMSans-Medium.ttf"
import "../assets/fonts/DMSans-Bold.ttf"

const Chart3 = ({props}) => {
    console.log('props now', props);
    useLayoutEffect(() => {
        let root = am5.Root.new(props);

        // Set themes
        // https://www.amcharts.com/docs/v5/concepts/themes/
        root.setThemes([
            am5themes_Animated.new(root),
        ]);

        // Create chart
        // https://www.amcharts.com/docs/v5/charts/xy-chart/
        let chart = root.container.children.push(am5xy.XYChart.new(root, {
            panX: true,
            panY: false,
            wheelX: "panX",
            wheelY: "zoomX",
            layout: root.verticalLayout,
            // maxTooltipDistance: 0
        }));

        // Add legend
        // https://www.amcharts.com/docs/v5/charts/xy-chart/legend-xy-series/
        let legend = chart.children.push(
            am5.Legend.new(root, {
            centerX: am5.p50,
            x: am5.p50
            })
        );

        legend.labels.template.setAll({
            // fill: am5.color(0xFFFFFF),
            fontSize: '1.5vh',
            fill: am5.color('#FFF'),
            fontFamily: 'dm-med',
            // fontSize: "1.5vh",
            // fontSize: "9",
            // lineHeight: 1,
            // x: am5.percent(10),
            // marginLeft: -10,
            marginRight: -40
        })

        let data = [
            {
            "low": 2,
            "medium": 8,
            "high": 8,
            "total_ticket": 18,
            "sla_condition": "In SLA"
            },
            {
            "low": 14,
            "medium": 7,
            "high": 8,
            "total_ticket": 29,
            "sla_condition": "Nearly SLA"
            },
            {
            "low": 13,
            "high": 8,
            "medium": 9,
            "total_ticket": 30,
            "sla_condition": "Exceed SLA"
            },
        ]

        let xRenderer = am5xy.AxisRendererX.new(root, {
            cellStartLocation: 0.1,
            cellEndLocation: 0.9,
            minGridDistance: 5,
            pan: "zoom"
        })

        let xAxis = chart.xAxes.push(am5xy.CategoryAxis.new(root, {
            categoryField: "sla_condition",
            renderer: xRenderer,
            tooltip: am5.Tooltip.new(root, {
            // fill: am5.color(0xeeeeee)
            }),
        }));

        xAxis.get("renderer").labels.template.setAll({
            fontSize: '1.5vh',
            fill: am5.color('#FFF'),
            fontFamily: 'dm-med',
            // fontFamily: 'Roboto',
            // fontSize: "1.5vh",
            // fontSize: "9",
            // marginLeft:10,
            // fill: am5.color(0xFFFFFF),
            // rotation: 45
            // centerY: am5.p50,
            // centerX: am5.p50
        })

        xAxis.data.setAll(data);

        let yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root, {
            maxPrecision: 0,
            renderer: am5xy.AxisRendererY.new(root, {
            strokeOpacity: 0.1
            }),
        }));

        yAxis.get("renderer").labels.template.setAll({
            fontSize: '1.5vh',
            fill: am5.color('#FFF'),
            fontFamily: 'dm-med',
            // fontFamily: 'Roboto',
            // fontSize: "1.5vh",
            // fontSize: "9",
            // fill: am5.color(0xFFFFFF)
        })

        // Add series
        // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
        const makeSeries = (fieldName, name, color) => {
            let tooltip = am5.Tooltip.new(root, {
            getFillFromSprite: true,
            getStrokeFromSprite: false,
            autoTextColor: am5.color(0x000000),
            getLabelFillFromSprite: false,
            labelText:"{name}:{valueY}"
            });
            
            tooltip.get("background").setAll({
            fillOpacity: 0.8
            });
            
            let series = chart.series.push(am5xy.ColumnSeries.new(root, {
            name: name,
            xAxis: xAxis,
            yAxis: yAxis,
            valueYField: fieldName,
            categoryXField: "sla_condition",
            fill: color,
            stroke:am5.color(0x000000),
            tooltip: tooltip,
            // stacked: true
            }));

            series.set("tooltip", tooltip);

            series.columns.template.setAll({
            width: am5.percent(80),
            tooltipY: 0,
            strokeOpacity: 0,
            });

            series.data.setAll(data);
            
            // Make stuff animate on load
            // https://www.amcharts.com/docs/v5/concepts/animations/
            series.appear(1000).then(x => {
                // temp1._logo._display.getCanvas()
                // temp2.style.display = "none"
                root._logo?.hide();
                (root)._handleLogo = () => { console.log('-'); };
            });

            legend.data.push(series);
        }

        let series2 = chart.series.push(
            am5xy.LineSeries.new(root, {
            name: 'Total Ticket',
            xAxis: xAxis,
            yAxis: yAxis,
            valueYField: "total_ticket",
            categoryXField: "sla_condition",
            stroke:am5.color(0xFFFFFF),
            tooltip: am5.Tooltip.new(root, {
                pointerOrientation: "horizontal",
                labelText: "{name}: {valueY} {info}"
            })
            
            })
        );

        series2.strokes.template.setAll({
            strokeWidth: 3,
            templateField: "strokeSettings",
        });

        series2.data.setAll(data);

        series2.bullets.push(() => {
            return am5.Bullet.new(root, {
            sprite: am5.Circle.new(root, {
                strokeWidth: 3,
                stroke: series2.get("stroke"),
                radius: 3,
                fill: am5.color(0x000000),
            })
            });
        });

        chart.set("cursor", am5xy.XYCursor.new(root, {
            behavior: 'zoomX'
        }));

        legend.data.push(series2);

        makeSeries("high", "High",'#F24040');
        makeSeries("medium", "Med",'#F1663D');
        makeSeries("low", "Low",'#8FEB7C');

        chart.set("cursor", am5xy.XYCursor.new(root, {
            // behavior: "zoomXY",
            // behavior: "",
            xAxis: xAxis
        }));
      
        // Make stuff animate on load
        // https://www.amcharts.com/docs/v5/concepts/animations/
        chart.appear(1000).then(x => {
            root._logo?.hide();
            (root)._handleLogo = () => { console.log('-'); };
        });
  
        return () => {
        root.dispose();
        };
    }, []);

    return(
        <>
            <div id={props} style={{ width: "100%", height: "45vh" }}></div>
        </>
    )
}

export default Chart3