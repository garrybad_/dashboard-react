import * as am5 from "@amcharts/amcharts5";
import * as am5xy from "@amcharts/amcharts5/xy";
import am5themes_Animated from "@amcharts/amcharts5/themes/Animated";
import React, { Component, useLayoutEffect } from "react";
import "../assets/fonts/DMSans-Regular.ttf"
import "../assets/fonts/DMSans-Medium.ttf"
import "../assets/fonts/DMSans-Bold.ttf"

const Chart1 = ({props}) => {
    console.log('props now', props);
    useLayoutEffect(() => {
      let root = am5.Root.new(props);

      // Set themes
      // https://www.amcharts.com/docs/v5/concepts/themes/
      root.setThemes([
        am5themes_Animated.new(root)
      ]);
      
      
      // Create chart
      // https://www.amcharts.com/docs/v5/charts/xy-chart/
      let chart = root.container.children.push(am5xy.XYChart.new(root, {
        panX: false,
        panY: false,
        wheelX: "panX",
        wheelY: "zoomX",
        layout: root.verticalLayout
      }));
      
      
      // Add legend
      // https://www.amcharts.com/docs/v5/charts/xy-chart/legend-xy-series/
      let legend = chart.children.push(
        am5.Legend.new(root, {
          centerX: am5.p50,
          x: am5.p50,
        })
      );

      legend.labels.template.setAll({
        fontSize: '1.5vh',
        fill: am5.color('#FFF'),
        fontFamily: 'dm-med',
        // fill: am5.color(0xFFFFFF),
        // fontSize: "1.5vh",
        // fontSize: "9",
        // lineHeight: 1,
        // x: am5.percent(10),
        // marginLeft: -10,
        marginRight: -40
      })
      
      let data = [{
        "year": "2021",
        "europe": 2.5,
        "namerica": 2.5,
        "asia": 2.1,
        "lamerica": 1,
        "meast": 0.8,
        "africa": 0.4
      }, {
        "year": "2022",
        "europe": 2.6,
        "namerica": 2.7,
        "asia": 2.2,
        "lamerica": 0.5,
        "meast": 0.4,
        "africa": 0.3
      }, {
        "year": "2023",
        "europe": 2.8,
        "namerica": 2.9,
        "asia": 2.4,
        "lamerica": 0.3,
        "meast": 0.9,
        "africa": 0.5
      }]
      
      
      // Create axes
      // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
      let xRenderer = am5xy.AxisRendererX.new(root, {
        cellStartLocation: 0.1,
        cellEndLocation: 0.9
      })
      
      let xAxis = chart.xAxes.push(am5xy.CategoryAxis.new(root, {
        categoryField: "year",
        renderer: xRenderer,
        tooltip: am5.Tooltip.new(root, {})
      }));
      
      xRenderer.grid.template.setAll({
        location: 1,
      })

      xRenderer.labels.template.setAll({
        fontSize: '1.5vh',
        fill: am5.color('#FFF'),
        fontFamily: 'dm-reg'
        // fontWeight:"bolder",
        // fontStyle:"italic"
      });
      
      xAxis.data.setAll(data);

      let yRenderer = am5xy.AxisRendererY.new(root, {
        strokeOpacity: 0.1
      })
      
      let yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root, {
        renderer: yRenderer
      }));

      yRenderer.labels.template.setAll({
        fontSize: '1.5vh',
        fill: am5.color('#FFF'),
        fontFamily: 'dm-reg'
      });
      
      
      // Add series
      // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
      function makeSeries(name, fieldName, color) {
        let series = chart.series.push(am5xy.ColumnSeries.new(root, {
          name: name,
          xAxis: xAxis,
          yAxis: yAxis,
          valueYField: fieldName,
          categoryXField: "year",
          fill: color
        }));
      
        series.columns.template.setAll({
          tooltipText: "{name}, {categoryX}:{valueY}",
          width: am5.percent(90),
          tooltipY: 0,
          strokeOpacity: 0
        });
      
        series.data.setAll(data);
      
        // Make stuff animate on load
        // https://www.amcharts.com/docs/v5/concepts/animations/
        series.appear();
      
        series.bullets.push(function() {
          return am5.Bullet.new(root, {
            locationY: 0,
            sprite: am5.Label.new(root, {
              text: "{valueY}",
              fill: root.interfaceColors.get("alternativeText"),
              centerY: 0,
              centerX: am5.p50,
              populateText: true
            })
          });
        });
      
        legend.data.push(series);
      }
      
      makeSeries("Europe", "europe", "#41CFC6");
      makeSeries("North America", "namerica", "#FFB766");
      makeSeries("Asia", "asia", "#F24040");
      // makeSeries("Latin America", "lamerica");
      // makeSeries("Middle East", "meast");
      // makeSeries("Africa", "africa");
      
      // Make stuff animate on load
      // https://www.amcharts.com/docs/v5/concepts/animations/
      chart.appear(1000).then(x => {
          root._logo?.hide();
          (root)._handleLogo = () => { console.log('-'); };
      });
  
      return () => {
        root.dispose();
      };
    }, []);

    return(
        <>
            <div id={props} style={{ width: "100%", height: "45vh" }}></div>
        </>
    )
}

export default Chart1