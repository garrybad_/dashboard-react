import * as am5xy from "@amcharts/amcharts5/xy";
import am5themes_Animated from "@amcharts/amcharts5/themes/Animated";
import * as am5percent from "@amcharts/amcharts5/percent";
import * as am5 from "@amcharts/amcharts5";
import React, { Component, useLayoutEffect } from "react";
import "../assets/fonts/DMSans-Regular.ttf"
import "../assets/fonts/DMSans-Medium.ttf"
import "../assets/fonts/DMSans-Bold.ttf"

const Chart7 = ({props}) => {
    console.log('props now', props);
    useLayoutEffect(() => {
        let root = am5.Root.new(props);
        let myTheme = am5.Theme.new(root);

        // Set themes
        // https://www.amcharts.com/docs/v5/concepts/themes/

        myTheme.rule('Label').setAll({
            // fill: am5.color(0xffffff),
            // // fontSize: "8px",
            // fontSize: '0.7em',
            fontSize: '1.5vh',
            fill: am5.color('#FFF'),
            fontFamily: 'dm-med',
        });

        myTheme.rule('Grid').setAll({
        stroke: am5.color(0xffffff),
        strokeWidth: 1,
        });

        root.setThemes([am5themes_Animated.new(root), myTheme]);

        // Create chart
        // https://www.amcharts.com/docs/v5/charts/xy-chart/
        let chart = root.container.children.push(
        am5xy.XYChart.new(root, {
            panX: true,
            panY: true,
            wheelX: 'panX',
            wheelY: 'zoomX',
            layout: root.verticalLayout,
            // pinchZoomX:true
        })
        );

        let data = [
            {
            period: 'JAN',
            reg1: 10,
            reg2: 23,
            },
            {
            period: 'FEB',
            reg1: 12,
            reg2: 23,
            },
            {
            period: 'MAR',
            reg1: 8,
            reg2: 5,
            },
        ];

        // Add cursor
        // https://www.amcharts.com/docs/v5/charts/xy-chart/cursor/
        let cursor = chart.set(
        'cursor',
        am5xy.XYCursor.new(root, {
            behavior: 'none',
        })
        );
        cursor.lineY.set('visible', false);

        // Create axes
        // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
        let xRenderer = am5xy.AxisRendererX.new(root, {
        pan: 'zoom',
        minGridDistance: 5,
        });
        // xRenderer.grid.template.set('location', 0.5);
        xRenderer.labels.template.setAll({
        location: 0.5,
        multiLocation: 0.5,
        });

        let xAxis = chart.xAxes.push(
        am5xy.CategoryAxis.new(root, {
            categoryField: 'period',
            renderer: xRenderer,
            tooltip: am5.Tooltip.new(root, {}),
        })
        );

        xAxis.get('renderer').labels.template.setAll({
        // fontSize: "8px",
        fontSize: '0.7em',
        });

        xAxis.data.setAll(data);

        var yAxis = chart.yAxes.push(
        am5xy.ValueAxis.new(root, {
            maxPrecision: 0,
            renderer: am5xy.AxisRendererY.new(root, {
            // minGridDistance: 20
            }),
            // min: this.minValue,
            // max: this.maxValue
        })
        );

        // Add series
        // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
        let legend = chart.children.push(
        am5.Legend.new(root, {
            centerX: am5.p50,
            x: am5.p50,
            // layout: root.horizontalLayout,
            useDefaultMarker: true,
        })
        );

        // legend.markerRectangles.template.setAll({
        //   width: 10,
        //   height: 10,
        //   x: 10,
        //   y: 3,
        //   cornerRadiusTL: 10,
        //   cornerRadiusTR: 10,
        //   cornerRadiusBL: 10,
        //   cornerRadiusBR: 10,
        // });

        const createSeries = (name, field, warna) => {
        const series = chart.series.push(
            am5xy.SmoothedXLineSeries.new(root, {
            name: name,
            xAxis: xAxis,
            yAxis: yAxis,
            // stacked: true,
            valueYField: field,
            categoryXField: 'period',
            // tooltip: am5.Tooltip.new(root, {
            //   pointerOrientation: 'horizontal',
            //   labelText: '{categoryX}: {valueY}',
            // }),
            stroke: am5.color(warna),
            fill: am5.color(warna),
            })
        );

        series.strokes.template.setAll({
            strokeWidth: 3,
        });

        series.bullets.push(() => {
            return am5.Bullet.new(root, {
            sprite: am5.Circle.new(root, {
                strokeWidth: 3,
                stroke: series.get("stroke"),
                radius: 3,
                fill: am5.color(warna),
            })
            });
        });

        series.fills.template.set(
            'fillGradient',
            am5.LinearGradient.new(root, {
            stops: [
                {
                opacity: 0.7,
                },
                {
                opacity: 0.1,
                },
            ],
            rotation: 90,
            })
        );

        series.fills.template.setAll({
            fillOpacity: 1,
            visible: true,
        });

        // create hover state for series and for mainContainer, so that when series is hovered,
        // the state would be passed down to the strokes which are in mainContainer.
        series.set('setStateOnChildren', true);
        series.states.create('hover', {});

        series.mainContainer.set('setStateOnChildren', true);
        series.mainContainer.states.create('hover', {});

        series.data.setAll(data);
        legend.data.push(series);
        series.appear(1000);
        // legend.data.push(series);
        };

        createSeries('R1', 'reg1', '#4472c4');
        createSeries('R2', 'reg2', '#ed7d31');

        chart.appear(1000, 100).then((x) => {
        // temp1._logo._display.getCanvas()
        // temp2.style.display = "none"
        root._logo?.hide();
        (root)._handleLogo = () => {
            console.log('Logo amchart hide');
        };
        });

        return () => {
            root.dispose();
        };
    }, []);

    return(
        <>
            <div id={props} style={{ width: "100%", height: "45vh" }}></div>
        </>
    )
}

export default Chart7