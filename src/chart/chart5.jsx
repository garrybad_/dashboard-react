import * as am5 from "@amcharts/amcharts5";
import * as am5xy from "@amcharts/amcharts5/xy";
import am5themes_Animated from "@amcharts/amcharts5/themes/Animated";
import React, { Component, useLayoutEffect } from "react";
import "../assets/fonts/DMSans-Regular.ttf"
import "../assets/fonts/DMSans-Medium.ttf"
import "../assets/fonts/DMSans-Bold.ttf"

const Chart5 = ({props}) => {
    console.log('props now', props);
    useLayoutEffect(() => {
        let data = [
            {
              "range": "1",
              "avg_mttr_critical": 2,
              "avg_mttr_major": 6,
              "avg_mttr_minor": 9,
              "avg_mttr_low": 11,
              "base": 0,
            }, 
            {
              "range": "2",
              "avg_mttr_critical": 3,
              "avg_mttr_major": 7,
              "avg_mttr_minor": 8,
              "avg_mttr_low": 10,
              "base": 0,
            }, 
            {
              "range": "3",
              "avg_mttr_critical": 3,
              "avg_mttr_major": 8,
              "avg_mttr_minor": 10,
              "avg_mttr_low": 19,
              "base": 0,
            }, 
            {
              "range": "4",
              "avg_mttr_critical": 1,
              "avg_mttr_major": 4,
              "avg_mttr_minor": 7,
              "avg_mttr_low": 9,
              "base": 0,
            }, 
            {
              "range": "5",
              "avg_mttr_critical": 2,
              "avg_mttr_major": 5,
              "avg_mttr_minor": 8,
              "avg_mttr_low": 10,
              "base": 0,
            }
          ];

        let root = am5.Root.new(props);
        root.setThemes([
            am5themes_Animated.new(root)
        ]);

        // Create chart
        // https://www.amcharts.com/docs/v5/charts/xy-chart/
        let chart = root.container.children.push(
            am5xy.XYChart.new(root, {
                panX: true,
                panY: true,
                wheelX: "panX",
                wheelY: "zoomX",
                layout: root.verticalLayout
            })
        );

        // Add cursor
        // https://www.amcharts.com/docs/v5/charts/xy-chart/cursor/
        let cursor = chart.set("cursor", am5xy.XYCursor.new(root, {
            behavior: "none"
        }));
        cursor.lineY.set("visible", false);

        // Create axes
        // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
        let xRenderer = am5xy.AxisRendererX.new(root, {});
        // xRenderer.grid.template.set("location", 0.5);
        xRenderer.labels.template.setAll({
            // location: 0,
            // multiLocation: 0.5,
            fontSize: '1.5vh',
            fill: am5.color('#FFF'),
            fontFamily: 'dm-med',
            // fontSize:'1vh',
            // fill: am5.color('#FFF')
            // fontWeight:"bolder",
            // fontStyle:"italic"
        });

        let yRenderer = am5xy.AxisRendererY.new(root, {
            inversed: false,
        });
        // yRenderer.grid.template.set("location", 0.5);
        // renderer: am5xy.AxisRendererY.new(root, {
        //   inversed: true,
        // })

        yRenderer.labels.template.setAll({
            // location: 0.5,
            // multiLocation: 0.5,
            // fontSize:'1vh',
            // fill: am5.color('#FFF')
            fontSize: '1.5vh',
            fill: am5.color('#FFF'),
            fontFamily: 'dm-med',
            // fontWeight:"bolder",
            // fontStyle:"italic"
        });

        let xAxis = chart.xAxes.push(
            am5xy.CategoryAxis.new(root, {
                categoryField: "range",
                renderer: xRenderer,
                tooltip: am5.Tooltip.new(root, {})
            })
        );

        xAxis.data.setAll(data);

        let yAxis = chart.yAxes.push(
            am5xy.ValueAxis.new(root, {
                // maxPrecision: 0,
                // forceHidden:true,
            
                renderer: yRenderer
            })
        );

        // Add series
        // https://www.amcharts.com/docs/v5/charts/xy-chart/series/

        let legend = chart.children.push(am5.Legend.new(root, {
            centerX: am5.p50,
            reverseChildren: true,
            x: am5.p50,
            useDefaultMarker: true
        }));

        legend.labels.template.setAll({
            // fontSize: 9,
            // fontWeight: "500",
            // fill: am5.color('#FFF'),
            marginRight:-60,
            marginTop:50,
            fontSize: '1.5vh',
            fill: am5.color('#FFF'),
            fontFamily: 'dm-med',
        });

        function createSeries(name, field, color) {
            const radiusPx = 2;
            let series = chart.series.push(
                am5xy.LineSeries.new(root, {
                name: name,
                xAxis: xAxis,
                yAxis: yAxis,
                valueYField: field,
                categoryXField: "range",
                fill: color,
                stroke: color,
                tooltip: am5.Tooltip.new(root, {
                  pointerOrientation: "horizontal",
                  labelText: "[bold]{name}[/]: {valueY}"
                })
                })
            );

            //   if (colorEnd) {
            //     series.strokes.template.setAll({
            //       strokeGradient: am5.LinearGradient.new(root, {
            //         stops: [{
            //           color: am5.color(color),
            //         }, {
            //           color: am5.color(colorEnd),
            //         }]
            //       })
            //     });

            //     series.fills.template.setAll({
            //       strokeGradient: am5.LinearGradient.new(root, {
            //         stops: [{
            //           color: am5.color(color),
            //         }, {
            //           color: am5.color(colorEnd),
            //         }]
            //       })
            //     });
            //   }

            series.strokes.template.set("strokeWidth", 3);

            // create hover state for series and for mainContainer, so that when series is hovered,
            // the state would be passed down to the strokes which are in mainContainer.
            series.set("setStateOnChildren", true);
            series.states.create("hover", {});

            series.mainContainer.set("setStateOnChildren", true);
            series.mainContainer.states.create("hover", {});

            series.strokes.template.states.create("hover", {
                strokeWidth: 4
            });

            series.data.setAll(data);
            legend.data.push(series);
            series.appear(1000);
        
        }

        createSeries("Low", "avg_mttr_low",'#3567E8', '#5764E7');
        createSeries("Minor", "avg_mttr_minor",'#03CB98', '#15F280');
        createSeries("Major", "avg_mttr_major",  '#F6B400', '#F7A656');
        createSeries("Critical", "avg_mttr_critical", '#F61F6C', '#E3585A');
        
        // Add scrollbar
        // https://www.amcharts.com/docs/v5/charts/xy-chart/scrollbars/
        // chart.set("scrollbarX", am5.Scrollbar.new(root, {
        //   orientation: "horizontal",
        //   marginBottom: 20
        // }));
        chart.appear(1000).then(x => {
        // temp1._logo._display.getCanvas()
        // temp2.style.display = "none"
        root._logo?.hide();
        (root)._handleLogo = () => {  };
        })
  
        return () => {
            root.dispose();
        };
    }, []);

    return(
        <>
            <div id={props} style={{ width: "100%", height: "45vh" }}></div>
        </>
    )
}

export default Chart5